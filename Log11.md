**LOGBOOK WEEK 13 (21/1/2022)**

**Logbook Entry Number : 11**

**Group : Team 7**

_Subsystem : Design and Structure_


| Week 13 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals?|As previously scheduled, we will hand over our printed thruster arm components to the Flight System Integration (FSI) team this week for calibration and testing. Meanwhile, our thruster arm members convened to inflate the blimp for the thruster arm components' trial setting. FSI team member reported vibration and friction sounds in the motor holder after the test run while the motor was running at high speed.|
|2. What decisions did you/your team make in solving a problem?|We asked FSI to test-run all four motors to end the scenario in order to determine the root cause of the problem. Unfortunately, all of the motors are having the same problems. We discovered that the motor's head shaft was too close to the 3D printed motor holder hole. As a result, we chose to attach a spacer (washer) to the upper part of the motor.|
|3. How did you/your team make those decisions (method)?|Before attaching the motor to the motor holder, Arvind and I dicussed and decided to add a spacer to the motor.So i went and bought the screws and washers needed|
|4. Why did you/your team make that choice (justification) when solving the problem?|Why add spacer to solve this issue? because by adding spacer, the gap between motor and motor holder will increase. Thus, the motor head shaft will have more room to spin without producing friction noise with the hole wall of the motor holder.|
|5. What was the impact on you/your team/the project when you make that decision?|The vibration and friction noise were eliminated after added the spacer.|
|6. What is the next step?| Next, the FSI team will do the calibration process for the servo and motor.|
