**LOGBOOK WEEK 2(THURSDAY, 11/11/2021)**

**Group : Team 7**

**Logbook Entry Number : 02**

_Subsystem : Design and Structure_

| Week 4 | Description |
| ------ | ------ |
| 1. What is the agenda this week and what are my/our goals? | According to Mr.Fiqri, there is a vibration issue activated when the thruster arm starts to operate. |
| 2. What decisions did you/your team make in solving a problem? | On the next day, we physically visited the lab to have a visual on the thruster arm status. The vibration level on the thruster arm is very minimal till we can’t feel it without touching the arm. So, we suggested to change the joining method between airship and thruster arm to a much more stable joint with three legs. |
|3. How did you/your team make those decisions (method)?|We had brainstorming session regarding the solution to tackle the vibration issue in the thruster arm.|
|4. Why did you/your team make that choice (justification) when solving the problem?|All the connectors and supporting joins looks good. But, the joint between thruster arm and the airship looks not stable. That’s the root cause for the arm to move inward and outward when the motor starts to run. This gives a shaky visual when the motor is on.|
|5. What was the impact on you/your team/the project when you make that decision?|We haven’t discussed our solution method with Mr.Fiqri yet, we will proceed with this solution once get the feedback or approval from him.|
|6. What is the next step?|Our next step can be decided after discuss with Mr.Fiqri regarding our solution proposal for the vibration issue.|

