**LOGBOOK WEEK 6(Friday, 26/11/2021)**

**Logbook Entry Number : 04**

**Group : Team 7**

_Subsystem : Design and Structure_


| Week 6 | Description |
| ------ | ------ |
| 1. What is the agenda this week and what are my/our goals? | For this week, since our solution proposal was accepted by Mr.Fiqri, so we move to next step which is creating CAD drawing for our thruster arm components. |
| 2. What decisions did you/your team make in solving a problem? | We decided to spilt our task by 2 members for a component design. Myself and Arvind will be designing the servo & motor segment with new dimensions. Asfeena and Asyikin will be in-charge of thruster arm carbon rod and the ball joint part. Amir Hamzah will be designing the tripod leg(length of the leg and the distance from the attachment on the airship) |
| 3. How did you/your team make those decisions (method)? | We had a virtual meeting in the Zoom platform to discussed about the thruster arm design and seperate the CAD drawing task among our team member. |
|4. Why did you/your team make that choice (justification) when solving the problem?|To ease the workload for each member. At the same time, the workload were splited evenly so that everyone can contribute to this design task.|
|5. What was the impact on you/your team/the project when you make that decision?|We showed our separated CAD drawing file to Mr.Fiqri and he said that everthing looks good and need to make little change on the ball joint CAD drawing.|
|6. What is the next step?|So, our next step is to assemble all the CAD drawing together and come out with one CAD file by 30/11/2021. If our assemble task completed within the targeted date, then we can start with the 3D printing from 2/12/2021 onwards.|
