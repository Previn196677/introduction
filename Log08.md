**LOGBOOK WEEK 10 (31/12/2021)**

**Group : Team 7**

**Logbook entry 08**

_Subsystem : Design and Structure_

| WEEK 10 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals?|This week, Arvind was required to compare the dimensions of newly printed parts to the dimensions of the brand new motor and servo. Unfortunately, several students who tested positive for the COVID-19 virus have been visiting the laboratory for the past few days. As a result of the sanitation process, the Faculty has ordered the laboratory to be closed for one week.|
|2. What decisions did you/your team make in solving a problem?|We were unable to advance our project to the next stage during the stoppage period. This week's agenda has been postponed. As a result, we chose to focus our thruster arm design group's report.|
|3. How did you/your team make those decisions (method)?|We split the report work among ourselves. Each and everyone has their own contribution to the report task.|
|4. Why did you/your team make that choice (justification) when solving the problem?|Since the laboratory shutdown for a week, we couldn't do anything physically. So, we had plenty of time to start the report progress.|
|5. What was the impact on you/your team/the project when you make that decision?|Started the report progress|
|6. What is the next step?|Our next move is to complete this week actual agenda in the upcoming week.|
