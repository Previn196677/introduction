**LOGBOOK WEEK 11 (7/1/2022)**

**Logbook Entry Number : 09**

**Group : Team 7**

_Subsystem : Design and Structure_

| Week 11 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals?|We need to double-check the dimensions of the printed parts with the brand-new motor and servo this week. If a mismatch dimension error occurs, I will make changes to the CAD drawing and reprint.|
|2. What decisions did you/your team make in solving a problem?|The dimensions for the motor holder were perfect, but the printed section bowed somewhat owing to the weight of the motor. The thruster arm rod fit exactly inside the printed hollow cylinder for the servo holder, but the servo mount dimension was not as precise.|
|3. How did you/your team make those decisions (method)?|As a result, we decided to change the CAD file and reprint the parts for the next inspection. In order to hold the motor without bending, the thickness of the printed section for the motor holder must be increased.|
|4. Why did you/your team make that choice (justification) when solving the problem?|Because the only other option is to print again. We can't directly edit the printed parts because doing so would compromise the part's effectiveness and strength. The CAD file must be modified before printing.|
|5. What was the impact on you/your team/the project when you make that decision?|The motor holder's CAD file has been updated and reprinted. The ultimate result was fantastic, and the printed section can retain the motor without bending. The CAD file for the motor holding section is now ready, and we can begin printing the last three pieces next week.|
|6. What is the next step?|For servo holder part, the CAD file haven't update yet. We will revise the CAD file and print again in the upcoming week.|
