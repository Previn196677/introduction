**LOGBOOK WEEK 5(Friday, 19/11/2021)**

**Logbook Entry Number : 03**

**Group : Team 7**

_Subsystem : Design and Structure_


| Week 5 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals? | The material selection for the thruster arm components. 2. Besides that, the solution for the vibration issue that we about to propose to Mr.Fiqri |
|  2. What decisions did you/your team make in solving a problem? |For the material selection, we finalised carbon fiber to use for the thruster arm fabrication. 2. Mr.Fiqri said that, changes the carbon rod base structure instant of changing the center base structure. |
|3. How did you/your team make those decisions (method)?|We discussed about the thruster arm material that we are going to use via the Zoom platform. 2. We had a virtual meeting held between thruster arm team and Mr. Amirul Fiqri regarding the solution for the vibration issue.|
|4. Why did you/your team make that choice (justification) when solving the problem?|As advise by Mr. Fiqri, changing the center base structure anyhow will make the arm movable and keep activating the vibration issue. Besides, he proposed that if we change the connector between carbon rod and the airship would be a great idea too.|
|5. What was the impact on you/your team/the project when you make that decision?|According to our google survey, the velcro strap is not an effective way to stick the carbon rod to the airship|
|6. What is the next step?|We suggested to use balljoint connector to make it effective than velcro strap|
