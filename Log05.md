**LOGBOOK WEEK 7(Friday, 3/12/2021)**

**Logbook Entry Number : 05**

**Group : Team 7**

_Subsystem : Design and Structure_


| Week 7 | Description |
| ------ | ------ |
| 1. What is the agenda this week and what are my/our goals? | For this week, we supposed to assemble all the seperated CAD files into one file and then show the final design to Mr.Fiqri for his confirmation before heading to 3D printing process. Unfortunately, CAD drawing for motor and servo placement is not completed yet due to some dimension error. The error specifically on the motor mounting part where we couldn't find the screw hole location for the motor. This is due to the dimension of this screw hole location is not found in the drawing file of the motor from the manufacturer company. |
| 2. What decisions did you/your team make in solving a problem? | We decided to bring this issue to Mr.Fiqri and ask for his opinion to resolve this problem. Besides, I personally suggested that except for the motor and servo placement CAD file, the rest such as thruster arm rod CAD, ball joint CAD and carbon rod CAD can proceed with 3D printing process. |
|3. How did you/your team make those decisions (method)?|Actually, myself and Arvind are in-charge for the motor and servo placement CAD drawing file. So, once we have found out this problem, we discussed this issue with the rest of the thruster arm members.|
|4. Why did you/your team make that choice (justification) when solving the problem?|This is because we doesn't want to wait for the whole thruster arm CAD file to be assembled and then proceed with the 3D printing process which will delay the progress.|
|5. What was the impact on you/your team/the project when you make that decision?|Ball joint, socket and carbon rod were 3D printed on 2/12/2021. The result was good but the dimension for the socket and the base rectangular plate need to revise again. We need to print again after done with the re-dimension process. For the motor screw hole location CAD issue, Mr.Fiqri advised us to bring this issue to flight system integration team and ask them to find solution for this dimension missing problem. He also reminded us that we are responsible for the designing tasks only.|
