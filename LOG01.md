**LOGBOOK WEEK 3(SUNDAY, 07/11/2021)**

**Group : Team 7**

**Logbook entry 01**

_Subsystem : Design and Structure_
 

| WEEK 3 | Description |
| ------ | ------ |
| 1. What is the agenda this week and what are my/our goals? | Did the Gantt chart for “Design and Structure” section and list down all the important process flow with dateline so that we will complete the list according to the project’s priority. | 
| 2. What decisions did you/your team make in solving a problem? | We had some brainstorming session regarding the overall airship design. Since the airship body design already available from the previous airship UAV, we focused more on the thruster arm and gondola design. |
| 3. How did you/your team make those decisions (method)? | We separated into 2 groups equally as thruster arm team and gondola team. A WhatsApp group created to work on the specific tasks effectively. |
| 4. Why did you/your team make that choice (justification) when solving the problem? | Since we are group of 9 people in Design and Structure team, we decided to split in to 2 teams where we can work more efficiently.  |
|5. What was the impact on you/your team/the project when you make that decision?|This method makes our work/design flow run smoothly and ease to track all those files and images without being mixed-up
|6. What is the next step?|Need to list down what are the suitable material available for our thruster arm components.|
