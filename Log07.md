**LOGBOOK WEEK 9(SUNDAY, 24/12/2021)**

**Group : Team 7**

**Logbook entry 07**

_Subsystem : Design and Structure_

| WEEK 9 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals?| For this week, we need to proceed with 3D printing as planned earlier. For ball joint part, Asfeena and Syikin already started to print. Besides, for motor and servo holder I and Previndran planned to print 1 set at the beginning and wait for the outcome.|
|2. What decisions did you/your team make in solving a problem?| Therea are two 3D printer located at H2.1 laboratory. Unfortunately, only one is working and another one printer was down for months. Since, only one printer is available then we need to take turn to print-out our parts accordingly.|
|3. How did you/your team make those decisions (method)?|We discussed this matter among our team members and came-out with a excel shedule sheet. The intention of this schedule is to booked the slot for 3D printer among ourself.|
|4. Why did you/your team make that choice (justification) when solving the problem?|Because even a small part will take approximately 3-4 hours to 3D print. Myself and Arvind motor and servor holder part is the hardest and biggest part compared to other member's part. Thus, our part will take 12-14 hours to print-out completely.|
|5. What was the impact on you/your team/the project when you make that decision?|The schedule method really works well as we planned. Usually, Asfeena, Syikin and Amir's part will take less than 6 hours to 3D print. But, for my part it will take 12-14 hours. So, they will print their part at the morning and approximately it will finish at afternoon before 4pm. After that, I will print my part and let it to be continue printing for the whole night. The next morning I will go and check the 3D printing outcome of my part|
|6. What is the next step?|Next, we need to double check the dimension details of my design with the brand new motor and servo item. If the item fitted well, then we will keep on print the initial design of my part. If there is any changes to be made for the dimensions, then we will alter our design and head back to the 3D printing process.|
