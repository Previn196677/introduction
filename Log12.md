**LOGBOOK WEEK 14 (28/1/2022)**

**Logbook Entry Number : 12**

**Group : Team 7**

_Subsystem : Design and Structure_

| Week 14 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals?|This was the final week of the IDP project, where we will launch the airship on Friday(28/01/2022). The balloon was inflated by the the FSI team beside the Lab H2.1 using helium gas. The the balloon was then brougt to the faculty hangar. The thruster arm team, gondola team, and control team started to istall the respective parts. After installing it, the airship was complete. Then, the airship were launched slowly inch by inch. The airship was controlled by Azizi and 4 students had control the rope. Then, for a brief while, an accident occurred, causing all of us to become stunted! The airship began to roll to one side as the wind grew stronger. To counteract the roll motion, the pilot attempted to move the airship in the opposite direction. Unfortunately, that operation did not succeed, as it increased the airship's roll angle and caused it to cross the stall angle. When the thruster arm carbon rod could no longer handle the enormous stress and force, one of the carbon rods broke. The main rod of the thruster arm lost its stability after it broke, and it was pointed towards the airship, where the propeller perforated the balloon by around 12cm. This incident has been captured on video. |
|2. What decisions did you/your team make in solving a problem?|The problem will be why does the airship does not counter act and why the strong carbon rod had broken. The counter act programme did not work at that time and fro the carbon rod the velcro strap should be attached vertically so that more surface area could be attached|
|3. How did you/your team make those decisions (method)?|During the post-mortem meeting, the root cause of this incident has been discussed among the students and the supervisors as well.|
|4. Why did you/your team make that choice (justification) when solving the problem?|It's usual for an incident to occur during the first fly attempt. The more failures we have, the more fundamentally and practically strong we become. Actually, we did not fail; yet, we discovered that things do not always go as planned. After a disaster, a post-mortem meeting is required. So that everyone understands what the fundamental problem is and what steps need to be taken to rectify it.|
|5. What was the impact on you/your team/the project when you make that decision?|Dr. Salahuddin complimented us for successfully launching the airship into the skies today. He also congratulates us on completing this high-stakes project successfully. Although a respectable overall level has been achieved, there is still room for development.|
|6. What is the next step?|The junior batch will make a progress with this project|
