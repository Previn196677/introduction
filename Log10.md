**LOGBOOK WEEK 12 (14/1/2022)**

**Logbook Entry Number : 10**

**Group : Team 1**

_Subsystem : Design and Structure_

| Week 12 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals?|The CAD drawing for the servo holder has been changed and is ready to print this week, as scheduled. The job is to print the last 3 motor holder pieces and 4 servo holder components. However, the time available to produce these pieces has become limited for this week. Each motor holder will take about 4 hours to print. For one piece of servo holder, it will take 12 hours to print completely.|
|2. What decisions did you/your team make in solving a problem?|Because there is only one 3D printer, we must carefully plan our time management strategy in order to print all of my parts while also allowing other members to utilise the 3D printer.|
|3. How did you/your team make those decisions (method)?|Arvind and I requested that other members print their parts in the morning until late afternoon. We will begin printing my section after late afternoon, about 3 p.m., and leave it to print overnight. To speed up the printing process, we might print two sections together.|
|4. Why did you/your team make that choice (justification) when solving the problem?|Because as per discussed in the discord platform, our thruster arm team has promised Dr. Salahuddin to finish printing process by Wednesday and start to assemble it on Thursady.|
|5. What was the impact on you/your team/the project when you make that decision?|This week, we were able to complete the printing process for the entire thruster arm. We are pleased with the end result|
|6. What is the next step?|The next step is to put together all four sets of thruster arms, each with its own servo and motor. We'll have to check how much the thruster arm vibrates once it's completely filled.|
