**LOGBOOK WEEK 8(Friday, 17/12/2021)**

**Logbook Entry Number : 06**

**Group : Team 7**

_Subsystem : Design and Structure_


| Week 7 | Description |
| ------ | ------ |
|1. What is the agenda this week and what are my/our goals?| For this week, myself and Arvind were supposed to finish the CAD drawing for the motor and servo placement body. As mentioned in LOGBOOK 05, we were facing motor hole location issue which delays the CAD process. Even, after discussed with the FSI team we couldn't managed to get permanent solution for it.|
|2. What decisions did you/your team make in solving a problem?|Arvind suggested to wait for the motor and servo to arrive and we can visualise the items by ourself.|
|3. How did you/your team make those decisions (method)?|We get to know from Mr. Fiqri that all the new items will be arrrive to the laboratory on Wedneesday (15/12/2021). So, there is a great chance for us to visualise the items by ourself.|
|4. Why did you/your team make that choice (justification) when solving the problem?|The root cause of this hole issue is because the old motor and servo dimensions were not same with the new motor and servo. the specification of the new items that we obtained from their respective website is not completed. If the dimension informations were completed then this issue will not be appeared. Thus, we waited for the new items to be arrived and take real dimensions of the new items by ourself.|
|5. What was the impact on you/your team/the project when you make that decision?|The impact is on positive side because we managed to get what we were looking for the past 2 weeks. After visualise the new motor and servo by ourself, we use measuring tools such as vernier caliper and ruler to obtain the real dimensions of it.|
|6. What is the next step?|The dimension process for the CAD drawing were completed and we are getting ready for the 3D printing process which was planned to do it on week 9.|
